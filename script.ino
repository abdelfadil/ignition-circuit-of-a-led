const int led = 7;
const int bouton = 8;
int etatBouton;

void setup()
{
  pinMode(led, OUTPUT);
  pinMode(bouton, INPUT);
}

void loop()
{
  etatBouton = digitalRead(bouton);
  if(etatBouton == HIGH){
    digitalWrite(led, HIGH);
  }else{
  	digitalWrite(led, LOW);
  }
}